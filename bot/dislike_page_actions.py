from bs4 import BeautifulSoup
from random import choices
from random import randint
from time import sleep

import time

def dislike_page_actions(browser):
    # Смотрит как листаются фотки
    time_to_view = randint(0,1)
    sleep(time_to_view)
    alert = browser.getAlert()
    if (alert == None) :
        browser.execute_script("SafeSwipe('DISLIKE');")
        print('[ - ] No alert ... ')
    else :
        alert.accept()
        print('[ + ] Got alert ... ')
    sleep(1)
    return