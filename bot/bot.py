from random import choices, randint
from browsers_driver import browser

from index_page_actions import index_page_actions
from login_page_actions import login_page_actions
from change_profile_page_actions import change_profile_page_actions

from dislike_page_actions import dislike_page_actions

from like_page_actions import like_page_actions

from matches_page_actions import matches_page_actions

from profile_page_actions import profile_page_actions

from registration_page_actions import registration_page_actions

from info import *

from time import sleep

import sys

# ['Home', 'My matches', 'My profile', 'Change profile', 'Logout', 'like', 'dislike']

def TakeChoice(choise_number) :
    if choise_number == 0 : # Пользователь только вошел => ставит лайк
        return [5, 5, 5, 5, 5, 40, 35]
    elif choise_number == 1 : # Пользователь сделал 1 действие
        return [10, 30, 10, 10, 10, 20, 10]
    else : # Пользователь сделал 2 действа и более
        return [10, 30, 10, 5, 30, 10, 5]
        
def bot_entry_point(isCreateUsers):
    utm_labels =    [localhost + '?utm_source=google&utm_medium=cpc&utm_campaign=1xbet',
                     localhost + '?utm_source=yandex&utm_medium=cpc&utm_campaign=1xbet',
                     localhost + '?utm_source=vk&utm_medium=social_post&utm_campaign=1xbet',
                     localhost + '?utm_source=instagam&utm_medium=social_post&utm_campaign=1xbet' ,
                     localhost + '?utm_source=facebook&utm_medium=social_post&utm_campaign=1xbet']

    labels_probability = [30, 30, 20, 15, 5]

    url = choices(utm_labels, weights = labels_probability).pop()

    
    print("[ * ] Bot start working...")

    if isCreateUsers == True :
        i = 0 
        
        google_chrome = browser()
        google_chrome.get(url)

        while i < 100:
            registration_ref = "http://localhost/KeepUp/registration.php"
            google_chrome.get(registration_ref)
            registration_page_actions(google_chrome, i)
            i += 1

        google_chrome.quit()
        print('[ + ] Finish registartion ')
    
    pages_list  = ['Home', 'My matches', 'My profile', 'Change profile', 'Logout', 'like', 'dislike']
        
    while True :
        i = randint(1 , 100)
        google_chrome = browser()
        google_chrome.get(url)
        login_ref = "http://localhost/KeepUp/login.php"
        google_chrome.get(login_ref)
        login_page_actions(google_chrome, i)

        
        print('[ * ] USER ' + str(i) + ' START WORK ! \n\n')
        
        choise_number = 0
        # Бот делает выбор
        while 1 :
            pages_probability = TakeChoice(choise_number)
            choice = choices(pages_list, weights = pages_probability).pop()
            choise_number += 1
            sleep(3)
            alert = google_chrome.getAlert()
            if (alert == None) :
                print('[ - ] No main alert ... ')
            else :
                alert.accept()
                print('[ + ] Got main alert ... ')

            if choice == 'Home':
                google_chrome.get("http://localhost/KeepUp/index.php")
                index_page_actions(google_chrome)                     
                print("Index_page_actions execution is end ...")            
            elif choice == 'My matches':
                google_chrome.get("http://localhost/KeepUp/matches.php")
                matches_page_actions(google_chrome)            
                print("Matches_page_actions execution is end ...")            
            elif choice == 'My profile':
                google_chrome.get("http://localhost/KeepUp/profile.php")
                profile_page_actions(google_chrome)  
                print("Profile_page_actions execution is end ...")            
            elif choice == 'Change profile':
                google_chrome.get("http://localhost/KeepUp/changeprofile.php")
                change_profile_page_actions(google_chrome, 0)
                print("Change_profile_page_actions execution is end ...")
            elif choice == 'Logout':
                google_chrome.get("http://localhost/KeepUp/logout.php")
                print("Logout_page_actions execution is end ...")
                break
            elif choice == 'like':
                google_chrome.get("http://localhost/KeepUp/member.php")
                sleep(1)
                like_page_actions(google_chrome)
                print("Like_page_actions execution is end ...")
            elif choice == 'dislike':
                google_chrome.get("http://localhost/KeepUp/member.php")
                sleep(1)
                dislike_page_actions(google_chrome)
                print("dislike_page_actions execution is end ...")
        google_chrome.quit()
        print('[ * ] USER ' + str(i) + ' FINISH WORK ! \n\n')
        i += 1


def main():
    if len(sys.argv) < 2 :
        print("[ - ] Error then start program ! Start programm like: python <program name> create_user/not_create_user ! ")
        return 
    if sys.argv[1] == 'create_user' :
        bot_entry_point(True)
    elif sys.argv[1] == 'not_create_user' :
        bot_entry_point(False)
    else : 
        print("[ - ] Error then start program ! Start programm like: python <program name> create_user/not_create_user ! ")

if __name__ == '__main__':
    main()
