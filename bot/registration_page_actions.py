from random import randint
import names

# Проводит авторизацию на старинце "войти"
def registration_page_actions(browser, id):
    # Получили поля ввода и кнопку входа
    login_input = browser.get_elem_by_name('login')
    passw_input = browser.get_elem_by_name('password')
    login_buttn = browser.get_elem_by_name('submit')
    first_name_input = browser.get_elem_by_name('firstname')
    last_name_input = browser.get_elem_by_name('lastname')
    birth_day_input = browser.get_elem_by_name('birthday')
    about_you_input = browser.get_elem_by_name('aboutyou')
    private_info_input = browser.get_elem_by_name('privateinfo')
    loginName = 'User' + str(id)
    password = '1234' + str(id)

    # Вошли
    login_input.send_keys(loginName)
    passw_input.send_keys(password)
    # Generate gender & name
    gender = randint(0,100) % 2 
    
    if gender == 1 :
        browser.findElementByID(gender)
        first_name_input.send_keys(names.get_first_name(gender='male'))
    else :
        browser.findElementByID(gender)
        first_name_input.send_keys(names.get_first_name(gender='female'))
        
    # Generate lastname
    last_name_input.send_keys(names.get_last_name())
    # Generate date
    day = randint(1,28)
    mount = randint(1,12)
    year = randint(1980,2000)
    birthdaydate = str(day) + '-' + str(mount) + '-' + str(year)
    print('[ + ] bday : ' + birthdaydate + '\n')
    birth_day_input.send_keys(birthdaydate)
    # Generate aboutyou
    about_you_input.send_keys('Hi all, follow me')
    private_info_input.send_keys('88005553535')
    if gender == 0:
        photoid = randint(1,71)
        photoid2 = randint(1,71)
        browser.get_elem_by_class_name("upload-photo").send_keys("C:\\Apache24\\htdocs\\KeepUp\\bot\\girls\\"+str(photoid)+".jpg")
        browser.get_elem_by_class_name("upload-photo").send_keys("C:\\Apache24\\htdocs\\KeepUp\\bot\\girls\\"+str(photoid2)+".jpg")
    else:
        photoid = randint(1,46)
        photoid2 = randint(1,46)
        browser.get_elem_by_class_name("upload-photo").send_keys("C:\\Apache24\\htdocs\\KeepUp\\bot\\mans\\"+str(photoid)+".jpg")
        browser.get_elem_by_class_name("upload-photo").send_keys("C:\\Apache24\\htdocs\\KeepUp\\bot\\mans\\"+str(photoid2)+".jpg")
    
    
    login_buttn.send_keys('submit')
    login_buttn.click()
    