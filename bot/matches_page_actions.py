from bs4 import BeautifulSoup
from random import choices, randint

from profile_page_actions import profile_page_actions
from info import *
from time import sleep
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as EC

def matches_page_actions(browser):
    # Смотрит на фотки
    time_to_view = randint(0,1)
    sleep(time_to_view)
    sleep(2)
    alert = browser.getAlert()
    if (alert == None) :
        print('[ - ] No matches alert ... ')
    else :
        alert.accept()
        print('[ + ] Got matches lert ... ')
    return
