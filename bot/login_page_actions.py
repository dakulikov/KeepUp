from random import randint
from change_profile_page_actions import change_profile_page_actions
from time import sleep
# Проводит авторизацию на старинце "войти"
def login_page_actions(browser, id):
    # Получили поля ввода и кнопку входа
    login_input = browser.get_elem_by_name('login')
    passw_input = browser.get_elem_by_name('password')
    login_buttn = browser.get_elem_by_name('submit')

    # Выброли случайного пользователя, от чего имени заходить
    login = 'User' + str(id)
    password = '1234' + str(id)

    # Вошли
    login_input.send_keys(login)
    passw_input.send_keys(password)
    login_buttn.click()
    alert = browser.getAlert()
    if (alert == None) :
        print('[ - ] No login alert ... ')
    else :
        alert.accept()
        print('[ + ] Got login alert ... ')