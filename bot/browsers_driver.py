from selenium import webdriver
from selenium.webdriver.common.by import By
from os import getcwd

class browser(object):
    
    def __init__(self):
        #self.JSbrowser = webdriver.PhantomJS(getcwd() + "\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe")
        self.window = webdriver.Chrome()
        print("[ + ] Start browser...")

    '''
    def __del__(self):
        quit()
        print('[*] Quit browser...')
    '''
    def getDriver(self) :
        return self.window
    def getAlert(self) :
        try :
            return self.window.switch_to.alert
        except :
            return None
    
    def findElementByID(self , gender) :
        if gender == 1 :
            gender0 = self.window.find_element_by_id('gender0')
            self.window.execute_script("arguments[0].click();", gender0)
        else :
            gender1 = self.window.find_element_by_id('gender1')
            self.window.execute_script("arguments[0].click();", gender1)
        return
    
    def checkGenderRadioButton(self) :
        return self.window.find_element_by_id('gender0').is_selected()
    
    # Переходит по ссылке
    def get(self, link):
        self.window.get(link)
    def request(self, link, dataset):
        return self.window.request('POST', link, data= dataset)
    
    
    def execute_script(self, script):
        return self.window.execute_script(script)
    # Выключает браузер
    def quit(self):
        self.window.quit()

    # Возвращает html
    def get_html(self, link):
        self.window.get(link)
        return self.window.page_source
    
    # Возвращает url текущей вкладки
    def get_url(self):
        return self.window.current_url
    def alertAccept(self):
        return self.window.switchTo().alert().accept();
    def get_elem_by_value(self, name, value):
        return self.window.find_element_by_xpath("//input[@name='"+name+"' and @value='"+value+"']");
    # Возвращает элемент с текущей ссылки по классу
    def get_elem_by_class_name(self, class_name):
        return self.window.find_element_by_class_name(class_name)
    def get_elements_by_class_name(self, class_name):
        return self.window.find_elements_by_class_name(class_name)

    # Возвращает элемент с текущей ссылки по имени
    def get_elem_by_name(self, name):
        return self.window.find_element_by_name(name)
    def get_elements_by_name(self, name):
        return self.window.find_elements_by_name(name)


