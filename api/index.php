<?php
header('Content-type: text/html; charset=UTF-8');
if (count($_REQUEST)>0){
    require_once 'apiEngine.php';
    //echo var_dump($_REQUEST);
    //if brawser version
    if (isset($_REQUEST['apiandfunctionname']))
    {
        $APIEngine=new APIEngine($_REQUEST['apiandfunctionname'],$_REQUEST);
        echo $APIEngine->callApiFunction(); 
    }
    else
    {
        $jsonParamsString =  file_get_contents('php://input');
        $jsonParams = json_decode($jsonParamsString, true);
        $APIEngine=new APIEngine($jsonParams['apiandfunctionname'],$jsonParams);
        echo $APIEngine->callApiFunction(); 
    }
}else{
    $jsonError->error='No function called';
    echo json_encode($jsonError);
}
?>