package com.example.keepup;

import android.os.AsyncTask;
import android.util.Log;
import org.apache.commons.io.IOUtils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;



public class SendJSON extends AsyncTask<String, Void, String> {

    private int readTimeOut, connectTimeOut;
    public SendJSON(int ReadTimeOut, int ConnectTimeOut)
    {
        this.readTimeOut = ReadTimeOut;
        this.connectTimeOut = ConnectTimeOut;
    }

    @Override
    protected String doInBackground(String... params) {


        String data = "";
        String tocken = "";
        int response_code = 0;

        HttpURLConnection httpURLConnection = null;
        try {

            httpURLConnection = (HttpURLConnection) new URL(params[0]).openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setReadTimeout(this.readTimeOut);
            httpURLConnection.setConnectTimeout(this.connectTimeOut);

            if(params[1] != null)
            {
                httpURLConnection.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
                //httpURLConnection.setRequestMethod(params[2]);
                wr.writeBytes( params[1]);
                wr.flush();
                wr.close();
            }
            else
            {
                httpURLConnection.setDoOutput(false);
            }

            response_code = httpURLConnection.getResponseCode();
            InputStream in = httpURLConnection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(in);

            data = IOUtils.toString(inputStreamReader);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
        if (response_code != 200)
        {
            String aaa =  Integer.toString(response_code);
            return aaa;
        }
        return data;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        Log.e("TAG", result);
        //return result;
    }

}