package com.example.keepup.ui.login;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.keepup.IpAddress;
import com.example.keepup.ProfileHandler;
import com.example.keepup.R;
import com.example.keepup.SendJSON;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class MemberActivity extends AppCompatActivity {

    private static final String API_AND_FUNCTION_NAME = "apiandfunctionname";
    private static final String VERSION               = "version";
    private static final String MOBILE                = "mobile";

    private static final String APIPHOTO_GETPHOTO     = "apiphoto_getphoto";

    private static final String APIPHOTO_LIKE         = "apiphoto_like";
    private static final String GENID                 = "genid";

    private static final String SCRFTOCKEN = "csrftocken";
    private String csrftocken;
    private int            genId = 0;
    private ProgressDialog pDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        csrftocken = intent.getStringExtra("csrftocken");

        setContentView(R.layout.activity_member);
        ImageButton like = findViewById(R.id.like);
        ImageButton dislike = findViewById(R.id.dislike);

        /* Executes then user like partner*/
        dislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MemberActivity.this, MemberActivity.class);
                i.putExtra("csrftocken",csrftocken);
                startActivity(i);
                finish();
            }
        });
        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject request = new JSONObject();
                SendJSON sender = new SendJSON(1000000, 1000000);
                String result;
                String resultStatus;
                request = new JSONObject();
                try {
                    request.put(API_AND_FUNCTION_NAME, APIPHOTO_LIKE);
                    request.put(VERSION, MOBILE);
                    request.put(GENID, genId);
                    request.put(SCRFTOCKEN, csrftocken);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try{
                    String IP = new IpAddress().IP;
                    result = sender.execute(IP + "/KeepUp/api/index.php", request.toString(), "POST").get();
                }catch (InterruptedException e)
                {
                    e.printStackTrace();
                    return;
                }
                catch(ExecutionException e) {
                    e.printStackTrace();
                    return;
                }
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONObject responseObj = jsonObject.getJSONObject("response");
                    result = responseObj.getString("result");
                } catch (JSONException e) {
                    e.printStackTrace();
                    return;
                }
                /*Match between 2 users*/
                if (result.equals("MATCH"))
                {
                    pDialog = new ProgressDialog(MemberActivity.this);
                    pDialog.setTitle("Congratulation!");
                    pDialog.setMessage("You have a match!");
                    pDialog.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent i = new Intent(MemberActivity.this, MemberActivity.class);
                            i.putExtra("csrftocken",csrftocken);
                            startActivity(i);
                            finish();
                        }
                    });
                    pDialog.show();
                }
                else
                {
                    Intent i = new Intent(MemberActivity.this, MemberActivity.class);
                    i.putExtra("csrftocken",csrftocken);
                    startActivity(i);
                    finish();
                }
            }
        });
        buttonEffect(like);
        buttonEffect(dislike);

        Button profile = findViewById(R.id.Profile);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MemberActivity.this, ProfileActivity.class);
                i.putExtra("csrftocken",csrftocken);
                startActivity(i);
                finish();
            }
        });
        Button matches = findViewById(R.id.Matches);
        matches.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MemberActivity.this, MatchesActivity.class);
                i.putExtra("csrftocken",csrftocken);
                startActivity(i);
                finish();
            }
        });
        buttonEffectMain(profile);
        buttonEffectMain(matches);




        member();
    }


    public static void buttonEffectMain(View button){
        button.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        v.setBackgroundColor(Color.parseColor("#c8644b"));
                        v.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        v.setBackgroundColor(Color.parseColor("#fa7864"));
                        v.invalidate();
                        break;
                    }
                }
                return false;
            }
        });
    }

    public static void buttonEffect(View button){
        button.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        v.setBackgroundColor(Color.parseColor("#FFCC99"));
                        v.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        v.setBackgroundColor(Color.parseColor("#FFDAB9"));
                        v.invalidate();
                        break;
                    }
                }
                return false;
            }
        });
    }

    private void member() {
        ImageView photo = findViewById(R.id.memberImageView);
        TextView info = findViewById(R.id.memberInfo);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linearPhoto);
        ProfileHandler profileHandler = new ProfileHandler(APIPHOTO_GETPHOTO, info, photo, linearLayout, MemberActivity.this, 0, csrftocken);
        genId = profileHandler.handle();
        return;
    }

}

