package com.example.keepup.ui.login;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.keepup.IpAddress;
import com.example.keepup.R;
import com.example.keepup.SendJSON;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.concurrent.ExecutionException;

public class RegisterActivity extends AppCompatActivity {
    private static final String KEY_LOGIN = "login";
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_FIRST_NAME = "firstname";
    private static final String KEY_LAST_NAME = "lastname";
    private static final String KEY_GENDER = "gender";
    private static final String KEY_EMPTY = "";
    private static final String API_AND_FUNCTION_NAME ="apiandfunctionname";
    private static final String APIAUTH_LOGIN = "apiauth_registration";
    private static final String VERSION = "version";
    private static final String MOBILE = "mobile";
    private static final String PRIVATE_INFO="privateinfo";
    private static final String NOT_FILLED_YET="Not filled yet";
    private ProgressDialog pDialog;

    private EditText etLogin;
    private EditText etPassword;
    private EditText etFirstname;
    private EditText etLastname;
    private EditText etGender;
    private EditText etConfirmPassword;
    private EditText etFullName;
    private String usersLogin;
    private String firstname;
    private String lastname;
    private String gender;
    private String password;
    private String confirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        etLogin     = findViewById(R.id.etRegisterLogin);
        etPassword  = findViewById(R.id.etRegisterPassword);
        etFirstname = findViewById(R.id.etRegisterFirstname);
        etLastname  = findViewById(R.id.etRegisterLastname);
        etGender    = findViewById(R.id.etRegisterGender);
        etConfirmPassword = findViewById(R.id.etRegisterConfirmPassword);

        Button login    = findViewById(R.id.btnRegisterLogin);
        Button register = findViewById(R.id.btnRegister);

        //Launch Login screen when Login Button is clicked
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Retrieve the data entered in the edit texts
                usersLogin  = etLogin.getText().toString().trim();
                password    = etPassword.getText().toString().trim();
                firstname   = etFirstname.getText().toString().trim();
                lastname    = etLastname.getText().toString().trim();
                gender      = etGender.getText().toString().trim();
                confirmPassword = etConfirmPassword.getText().toString().trim();
                if (validateInputs()) {
                    registerUser();
                }

            }
        });

    }

    /**
     * Display Progress bar while registering
     */
    private void displayLoader() {
        pDialog = new ProgressDialog(RegisterActivity.this);
        pDialog.setMessage("Signing Up.. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

    }

    /**
     * Launch Dashboard Activity on Successful Sign Up
     */
    private void loadDashboard() {
        Intent i = new Intent(getApplicationContext(), DashboardActivity.class);
        startActivity(i);
        finish();

    }

    private void registerUser() {
        displayLoader();
        CookieManager CookieManage = new CookieManager();
        CookieHandler.setDefault(CookieManage);
        JSONObject request = new JSONObject();
        try
        {
            //Populate the request parameters
            request.put(KEY_LOGIN, usersLogin);
            request.put(KEY_PASSWORD, password);
            request.put(KEY_GENDER, gender);
            request.put(KEY_FIRST_NAME, firstname);
            request.put(KEY_LAST_NAME, lastname);
            request.put(API_AND_FUNCTION_NAME,APIAUTH_LOGIN);
            request.put(PRIVATE_INFO, NOT_FILLED_YET);
            request.put(VERSION, MOBILE);

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        SendJSON sender = new SendJSON(1000000, 1000000);
        String result;
        try
        {
            String IP = new IpAddress().IP;
            result = sender.execute(IP + "/KeepUp/api/index.php", request.toString(), "POST").get();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
            return;
        }
        catch(ExecutionException e)
        {
            e.printStackTrace();
            return;
        }
        String resultStatus = "";
        try
        {
            JSONObject jsonObject = new JSONObject(result);
            JSONObject responseObj = jsonObject.getJSONObject("response");
            resultStatus = responseObj.getString("result");
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        if (resultStatus.equals("OK"))
        {
            if(pDialog.isShowing())
                pDialog.cancel();
            pDialog = new ProgressDialog(RegisterActivity.this);
            pDialog.setTitle("Congratulation!");
            pDialog.setMessage("You are successfully registered!");
            pDialog.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            });
            pDialog.show();
        }
        else
        {
            if (pDialog.isShowing())
                pDialog.cancel();
            pDialog = new ProgressDialog(RegisterActivity.this);
            pDialog.setTitle("Error!");
            pDialog.setMessage("Where are error while registration, please try another login!");
            pDialog.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent i = new Intent(RegisterActivity.this, RegisterActivity.class);
                    startActivity(i);
                    finish();
                }
            });
            pDialog.show();
        }
        return;
    }

    /**
     * Validates inputs and shows error if any
     * @return
     */
    private boolean validateInputs() {
        if (KEY_EMPTY.equals(firstname)) {
            etFirstname.setError("First name cannot be empty");
            etFirstname.requestFocus();
            return false;
        }
        if (KEY_EMPTY.equals(lastname)) {
            etLastname.setError("Last name cannot be empty");
            etLastname.requestFocus();
            return false;

        }
        if (KEY_EMPTY.equals(usersLogin)) {
            etLogin.setError("Login cannot be empty");
            etLogin.requestFocus();
            return false;
        }

        if (!gender.equals("Man") && !gender.equals("Woman")) {
            etGender.setError("Please type Man or Woman");
            etGender.requestFocus();
            return false;
        }
        if (KEY_EMPTY.equals(password)) {
            etPassword.setError("Password cannot be empty");
            etPassword.requestFocus();
            return false;
        }

        if (KEY_EMPTY.equals(confirmPassword)) {
            etConfirmPassword.setError("Confirm Password cannot be empty");
            etConfirmPassword.requestFocus();
            return false;
        }
        if (!password.equals(confirmPassword)) {
            etConfirmPassword.setError("Password and Confirm Password does not match");
            etConfirmPassword.requestFocus();
            return false;
        }

        return true;
    }
}