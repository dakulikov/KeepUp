package com.example.keepup.ui.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.text.util.Linkify;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.keepup.IpAddress;
import com.example.keepup.R;
import com.example.keepup.SendJSON;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class MatchesActivity extends AppCompatActivity {

    private static final String API_AND_FUNCTION_NAME ="apiandfunctionname";
    private static final String CSRFTOCKEN            = "csrftocken";
    private static final String VERSION               = "version";
    private static final String MOBILE                = "mobile";

    private static final String APIPHOTO_GETMATCHES     = "apiphoto_getmatches";
    private static final String GENID                 = "genid";

    private static final int    DP_HEIGHT_PHOTO = 180;
    private static final int    DP_PADDING_TEXT = 10;
    private static final int    DP_SIZE_TEXT = 10;
    private static final int    DP_SIZE_TEXT_NAME = 16;
    private String csrftocken;
    private static final int    DP_BUTTON_WIDTH  = 120;
    private static final int    DP_BUTTON_HEIGHT = 50;
    private static final int    DP_BUTTON_MARGIN_LEFT = 15;
    private static final int    DP_BUTTON_MARGIN_TOP = 120;
    int id;

    private int            genId = 0;
    private ProgressDialog pDialog;
    public static void buttonEffectMain(View button){
        button.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        v.setBackgroundColor(Color.parseColor("#c8644b"));
                        v.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        v.setBackgroundColor(Color.parseColor("#fa7864"));
                        v.invalidate();
                        break;
                    }
                }
                return false;
            }
        });
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        csrftocken  = intent.getStringExtra("csrftocken");
        setContentView(R.layout.activity_matches);
        /* Executes then user like partner*/
        Button profile = findViewById(R.id.matchesProfile);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MatchesActivity.this, ProfileActivity.class);
                i.putExtra("csrftocken",csrftocken);
                startActivity(i);
                finish();
            }
        });
        Button pickup = findViewById(R.id.matchesPickup);
        pickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MatchesActivity.this, MemberActivity.class);
                i.putExtra("csrftocken",csrftocken);
                startActivity(i);
                finish();
            }
        });
        buttonEffectMain(profile);
        buttonEffectMain(pickup);

        matches();
    }
    public static int convertDpToPixel(float dp, Context context){
        return (int)(dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT));
    }
    private void matches() {
        JSONObject request = new JSONObject();
        String result;
        SendJSON sender = new SendJSON(1000000, 1000000);
        String resultStatus;
        try {
            request.put(API_AND_FUNCTION_NAME, APIPHOTO_GETMATCHES);
            request.put(VERSION, MOBILE);
            request.put(CSRFTOCKEN, csrftocken);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try{
            String IP = new IpAddress().IP;
            result = sender.execute(IP + "/KeepUp/api/index.php", request.toString(), "POST").get();
        }catch (InterruptedException e)
        {
            e.printStackTrace();
            return;
        }
        catch(ExecutionException e) {
            e.printStackTrace();
            return;
        }
        JSONArray matches = null;
        String lastname = null;
        String firstname = null;
        String aboutyou = null;
        String fullname= null;
        String privatinfo = null;
        try {
            JSONObject jsonObject = new JSONObject(result);
            JSONObject responseObj = jsonObject.getJSONObject("response");
            matches = responseObj.getJSONArray("matches");
        } catch (JSONException e) {
            e.printStackTrace();
            resultStatus = null;
        }
        for (int i = 0; i < matches.length(); i++) {
            try{
                JSONObject match = matches.getJSONObject(i);
                firstname        = match.getString("firstname");
                lastname         = match.getString("lastname");
                aboutyou         = match.getString("aboutyou");
                privatinfo       = match.getString("privatinfo");
                id               = Integer.parseInt(match.getString("id"));
                fullname = firstname + " " + lastname;

                /*decode image*/
                byte[] bytes = Base64.decode(match.getString("image"), Base64.DEFAULT);
                Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

                /*add avatar*/
                LinearLayout linearLayout = (LinearLayout) findViewById(R.id.matchesLinear);

                LinearLayout newLinearLayout = new LinearLayout(MatchesActivity.this);
                LinearLayout.LayoutParams newLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);
                newLinearLayout.setLayoutParams(newLayoutParams);

                    ImageView imgView = new ImageView(MatchesActivity.this );
                    int dp = convertDpToPixel(DP_HEIGHT_PHOTO, MatchesActivity.this);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(dp, dp);
                    dp = convertDpToPixel(DP_PADDING_TEXT, MatchesActivity.this);
                    lp.setMargins(dp,0, dp,0);
                    imgView.setLayoutParams(lp);
                    imgView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    newLinearLayout.addView(imgView);
                    imgView.setImageBitmap(bmp);

                    /*SET BUTTON TO PROFILE*/
                    Button newButton = new Button(MatchesActivity.this);
                    int dpWidth = convertDpToPixel(DP_BUTTON_WIDTH,MatchesActivity.this);
                    int dpHeight= convertDpToPixel(DP_BUTTON_HEIGHT, MatchesActivity.this);
                    newLayoutParams = new LinearLayout.LayoutParams(dpWidth,  dpHeight);
                    newLayoutParams.setMargins(DP_BUTTON_MARGIN_LEFT, DP_BUTTON_MARGIN_TOP,0,0);
                    newButton.setLayoutParams(newLayoutParams);
                    newButton.setBackgroundColor(Color.parseColor("#f05924"));
                    newButton.setText("Profile");
                    newButton.setTextColor(Color.parseColor("#ffffff"));
                    newLinearLayout.addView(newButton);
                    newButton.setId(id);
                    newButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(MatchesActivity.this, ProfileActivity.class);
                        i.putExtra("csrftocken",csrftocken);
                        i.putExtra("selectid",(Integer)v.getId());
                        startActivity(i);
                        finish();
                    }
                });


                newLinearLayout.setPadding(10,20,10,20);
                newLinearLayout.setBackgroundColor(Color.parseColor("#ffeede"));
                linearLayout.addView(newLinearLayout);


                newLinearLayout = new LinearLayout(MatchesActivity.this);
                newLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);
                newLinearLayout.setLayoutParams(newLayoutParams);
                newLinearLayout.setBackgroundColor(Color.parseColor("#faf4ed"));
                newLinearLayout.setOrientation(LinearLayout.VERTICAL);
                    /*SET FULL NAME*/
                    TextView textView = new TextView(MatchesActivity.this);
                    lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    textView.setLayoutParams(lp);
                    dp = convertDpToPixel(DP_PADDING_TEXT, MatchesActivity.this);
                    textView.setPadding(dp,0,dp,dp);
                    textView.setTextColor(Color.parseColor("#591815"));
                    dp = convertDpToPixel(DP_SIZE_TEXT_NAME, MatchesActivity.this);
                    textView.setTextSize(dp);
                    textView.setText(fullname);
                    newLinearLayout.addView(textView);


                    /*SET TEXT ABOUT TOU*/
                    textView = new TextView(MatchesActivity.this);
                    lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    textView.setLayoutParams(lp);
                    dp = convertDpToPixel(DP_PADDING_TEXT, MatchesActivity.this);
                    textView.setPadding(dp,0,dp,0);
                    textView.setTextColor(Color.parseColor("#691c00"));
                    dp = convertDpToPixel(DP_SIZE_TEXT, MatchesActivity.this);
                    textView.setTextSize(dp);
                    textView.setText(aboutyou);
                    newLinearLayout.addView(textView);

                    /*SET TEXT PRIVATEINFO*/
                    textView = new TextView(MatchesActivity.this);
                    lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    textView.setLayoutParams(lp);
                    dp = convertDpToPixel(DP_PADDING_TEXT, MatchesActivity.this);
                    textView.setPadding(dp,0,dp,0);
                    textView.setTextColor(Color.parseColor("#691c00"));
                    dp = convertDpToPixel(DP_SIZE_TEXT, MatchesActivity.this);
                    textView.setTextSize(dp);
                    textView.setText(privatinfo);
                    Linkify.addLinks(textView, Linkify.ALL);
                    newLinearLayout.addView(textView);


                    /*SET LINE*/
                    View view = new View(MatchesActivity.this);
                    dp = convertDpToPixel(6, MatchesActivity.this);
                    lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dp);
                    view.setLayoutParams(lp);
                    view.setBackgroundColor(Color.parseColor("#FA8072"));
                    newLinearLayout.addView(view);

                linearLayout.addView(newLinearLayout);
            }
            catch ( JSONException e )
            {
                e.printStackTrace();
            }
        }


        return;
    }
}
