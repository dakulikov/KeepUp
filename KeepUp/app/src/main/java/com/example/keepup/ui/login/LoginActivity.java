package com.example.keepup.ui.login;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.keepup.IpAddress;
import com.example.keepup.R;
import com.example.keepup.SendJSON;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.concurrent.ExecutionException;


public class LoginActivity extends AppCompatActivity {
    private static final String KEY_LOGIN = "login";
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_EMPTY = "";
    private static final String API_AND_FUNCTION_NAME ="apiandfunctionname";
    private static final String APIAUTH_LOGIN = "apiauth_login";
    private static final String VERSION = "version";
    private static final String MOBILE = "mobile";

    private static final String APIPHOTO_GETPHOTO = "apiphoto_getphoto";

    private EditText etUsername;
    private EditText etPassword;
    private String username;
    private String password;
    private ProgressDialog pDialog;
    private String csrftocken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUsername = findViewById(R.id.etLoginUsername);
        etPassword = findViewById(R.id.etLoginPassword);

        Button register = findViewById(R.id.btnLoginRegister);
        Button login = findViewById(R.id.btnLogin);

        //Launch Registration screen when Register Button is clicked
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(i);
                finish();
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Retrieve the data entered in the edit texts
                username = etUsername.getText().toString().trim();
                password = etPassword.getText().toString().trim();
                if (validateInputs()) {
                    login();
                }
            }
        });
    }

    /**
     * Launch Dashboard Activity on Successful Login
     */

    private void loadMember() {
        Intent i = new Intent(LoginActivity.this, MemberActivity.class);
        i.putExtra("csrftocken",csrftocken);
        startActivity(i);
        finish();
    }

    /**
     * Display Progress bar while Logging in
     */

    private void displayLoader() {
        pDialog = new ProgressDialog(LoginActivity.this);
        pDialog.setMessage("Logging In.. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    private void login() {
        displayLoader();
        CookieManager CookieManage = new CookieManager();
        CookieHandler.setDefault(CookieManage);
        JSONObject request = new JSONObject();
        String result;
        try {
            request.put(KEY_LOGIN, username);
            request.put(KEY_PASSWORD, password);
            request.put(API_AND_FUNCTION_NAME,APIAUTH_LOGIN);
            request.put(VERSION, MOBILE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        SendJSON sender = new SendJSON(1000000, 1000000);
        try{
            String IP = new IpAddress().IP;
            result = sender.execute(IP + "/KeepUp/api/index.php", request.toString(), "POST").get();
        }catch (InterruptedException e)
        {
            e.printStackTrace();
            return;
        }
        catch(ExecutionException e) {
            e.printStackTrace();
            return;
        }
        String resultStatus;
        try {
            JSONObject jsonObject = new JSONObject(result);
            JSONObject responseObj = jsonObject.getJSONObject("response");
            resultStatus = responseObj.getString("result");
            csrftocken = responseObj.getString("csrftocken");
        } catch (JSONException e) {
            e.printStackTrace();
            resultStatus = null;
        }

        if (resultStatus.equals("OK"))
        {
            loadMember();
        }
        else
        {
            if (pDialog.isShowing())
                pDialog.cancel();
            pDialog = new ProgressDialog(LoginActivity.this);
            pDialog.setTitle("Sorry, your login or password incorrect!");
            pDialog.setMessage("Try again!");
            pDialog.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent i = new Intent(LoginActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            });
            pDialog.show();
        }

        return;
    }

    /**
     * Validates inputs and shows error if any
     * @return
     */
    private boolean validateInputs() {
        if(KEY_EMPTY.equals(username)){
            etUsername.setError("Username cannot be empty");
            etUsername.requestFocus();
            return false;
        }
        if(KEY_EMPTY.equals(password)){
            etPassword.setError("Password cannot be empty");
            etPassword.requestFocus();
            return false;
        }
        return true;
    }
}