package com.example.keepup.ui.login;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.keepup.IpAddress;
import com.example.keepup.ProfileHandler;
import com.example.keepup.R;
import com.example.keepup.SendJSON;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class ProfileActivity extends AppCompatActivity {
    private static final String API_AND_FUNCTION_NAME = "apiandfunctionname";
    private static final String VERSION               = "version";
    private static final String MOBILE                = "mobile";
    private static final String APIAUTH_LOGOUT        = "apiauth_logout";
    private static final String APIPHOTO_GETPROFILE   = "apiphoto_getprofile";
    private static final String APIPHOTO_GETPROFILEPARTNER = "apiphoto_getprofilepartner";
    private static final String GENID                 = "genid";
    private static final String CSRFTOCKEN            = "csrftocken";
    private int                 genId = 0;
    private String csrftocken;
    public static void buttonEffectMain(View button){
        button.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        v.setBackgroundColor(Color.parseColor("#c8644b"));
                        v.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        v.setBackgroundColor(Color.parseColor("#fa7864"));
                        v.invalidate();
                        break;
                    }
                }
                return false;
            }
        });
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ImageButton like = findViewById(R.id.like);
        Button pickup = findViewById(R.id.profilePickup);
        pickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ProfileActivity.this, MemberActivity.class);
                i.putExtra("csrftocken",csrftocken);
                startActivity(i);
                finish();
            }
        });
        Button matches = findViewById(R.id.profileMatches);
        matches.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ProfileActivity.this, MatchesActivity.class);
                i.putExtra("csrftocken",csrftocken);
                startActivity(i);
                finish();
            }
        });
        Button exit = findViewById(R.id.profileExit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject request = new JSONObject();
                SendJSON sender = new SendJSON(1000000, 1000000);
                String result;
                String resultStatus;
                request = new JSONObject();
                try {
                    request.put(API_AND_FUNCTION_NAME, APIAUTH_LOGOUT);
                    request.put(VERSION, MOBILE);
                    request.put(CSRFTOCKEN, csrftocken);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try{
                    String IP = new IpAddress().IP;
                    result = sender.execute(IP + "/KeepUp/api/index.php", request.toString(), "POST").get();
                }catch (InterruptedException e)
                {
                    e.printStackTrace();
                    return;
                }
                catch(ExecutionException e) {
                    e.printStackTrace();
                    return;
                }
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONObject responseObj = jsonObject.getJSONObject("response");
                    result = responseObj.getString("result");
                } catch (JSONException e) {
                    e.printStackTrace();
                    return;
                }
                if (result.equals("OK"))
                {
                    Intent i = new Intent(ProfileActivity.this, LoginActivity.class);
                    i.putExtra("csrftocken",csrftocken);
                    startActivity(i);
                    finish();
                }
            }
        });
        buttonEffectMain(pickup);
        buttonEffectMain(matches);
        buttonEffectMain(exit);
        Intent intent = getIntent();
        int sendedId = intent.getIntExtra("selectid",0);
        csrftocken = intent.getStringExtra("csrftocken");
        profile(sendedId);
    }
    private void profile(int id) {
        if (id == 0) {
            ImageView photo = findViewById(R.id.profileImageView);
            TextView info = findViewById(R.id.profileInfo);
            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.profileLinearPhoto);
            ProfileHandler profileHandler = new ProfileHandler(APIPHOTO_GETPROFILE, info, photo, linearLayout, ProfileActivity.this, 0, csrftocken);
            genId = profileHandler.handle();
        }
        else
        {
            ImageView photo = findViewById(R.id.profileImageView);
            TextView info = findViewById(R.id.profileInfo);
            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.profileLinearPhoto);
            ProfileHandler profileHandler = new ProfileHandler(APIPHOTO_GETPROFILEPARTNER, info, photo, linearLayout, ProfileActivity.this, id, csrftocken);
            genId = profileHandler.handle();
        }
        return;
    }
}
