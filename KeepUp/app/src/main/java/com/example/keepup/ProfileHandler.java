package com.example.keepup;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class ProfileHandler {
    private static String API_AND_FUNCTION_NAME = "apiandfunctionname";
    private static String CSRFTOCKEN = "csrftocken";
    private static String SELECTID = "selectid";
    private String METHOD_NAME = null;
    private String VERSION = "version";
    private String MOBILE = "mobile";
    private int    genId = 0;
    private TextView m_memberInfoView;
    private ImageView m_memberImageView;
    private LinearLayout m_linearLayout;
    private Context  m_context;
    private int m_selectid;
    private String m_csrftocken;
    private static final int    DP_HEIGHT_PHOTO = 370;
    private static final int    DP_PADDING_TEXT = 20;
    private static final int    DP_SIZE_TEXT = 10;
    public ProfileHandler(String apiNameAndMethodName, TextView memberInfoView, ImageView memberImageView, LinearLayout linearLayout, Context context, int id, String csrftock)
    {
        m_csrftocken = csrftock;
        m_memberInfoView = memberInfoView;
        m_memberImageView = memberImageView;
        m_linearLayout = linearLayout;
        m_context = context;
        METHOD_NAME = apiNameAndMethodName;
        m_selectid = id;
    }
    public static int convertDpToPixel(float dp, Context context){
        return (int)(dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT));
    }
    public int handle()
    {
        JSONObject request = new JSONObject();
        String result;
        SendJSON sender = new SendJSON(1000000, 1000000);
        String resultStatus;
        request = new JSONObject();
        try {
            request.put(API_AND_FUNCTION_NAME, METHOD_NAME);
            request.put(VERSION, MOBILE);
            request.put(SELECTID, m_selectid);
            request.put(CSRFTOCKEN, m_csrftocken);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try{
            String IP = new IpAddress().IP;
            result = sender.execute(IP + "/KeepUp/api/index.php", request.toString(), "POST").get();
        }catch (InterruptedException e)
        {
            e.printStackTrace();
            return 0;
        }
        catch(ExecutionException e) {
            e.printStackTrace();
            return 0;
        }
        JSONArray images = null;
        String lastname = null;
        String firstname = null;
        String aboutyou = null;
        String fullname= null;
        try {
            JSONObject jsonObject = new JSONObject(result);
            JSONObject responseObj = jsonObject.getJSONObject("response");
            images = responseObj.getJSONArray("images");
            firstname = responseObj.getString("firstname");
            lastname = responseObj.getString("lastname");
            aboutyou = responseObj.getString("aboutyou");
            genId    = Integer.parseInt(responseObj.getString("genid"));
            fullname = firstname + " " + lastname;
        } catch (JSONException e) {
            e.printStackTrace();
            resultStatus = null;
        }
        TextView info = m_memberInfoView;
        info.setText(fullname);
        try {
            byte[] bytes = Base64.decode((images.get(0)).toString(), Base64.DEFAULT);
            Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            ImageView photo = m_memberImageView;
            photo.setImageBitmap(bmp);
        }
        catch ( JSONException e )
        {
            e.printStackTrace();
        }

        for (int i = 1; i < images.length(); i++) {
            try{
                byte[] bytes = Base64.decode((images.get(i)).toString(), Base64.DEFAULT);
                Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                LinearLayout linearLayout = m_linearLayout;
                ImageView imgView = new ImageView(m_context);
                int dp = convertDpToPixel(DP_HEIGHT_PHOTO, m_context);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dp);
                imgView.setLayoutParams(lp);
                imgView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                linearLayout.addView(imgView);
                imgView.setImageBitmap(bmp);
            }
            catch ( JSONException e )
            {
                e.printStackTrace();
            }
        }
        TextView textView = new TextView(m_context);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textView.setLayoutParams(lp);
        int dp = convertDpToPixel(DP_PADDING_TEXT, m_context);
        textView.setPadding(dp,dp,dp,dp);
        textView.setTextColor(Color.parseColor("#FA8072"));
        dp = convertDpToPixel(DP_SIZE_TEXT, m_context);
        textView.setTextSize(dp);
        textView.setText(aboutyou);
        LinearLayout linearLayout =  m_linearLayout;
        linearLayout.addView(textView);
        return genId;
    }

}
